CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration

INTRODUCTION
------------

The Webform Bulk Emails module adds a webform email handler to send submission emails in bulk.

INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Copy/upload the Webform Bulk Emails module to the modules directory of your Drupal
   installation.

 2. Enable the 'Webform Bulk Emails' module in 'Extend' (/admin/modules)

CONFIGURATION
-------------

Configure webform Email/Handlers settings for your webform.
