<?php

namespace Drupal\webform_bulk_email\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Plugin\WebformHandler\EmailWebformHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Emails webform submission in Bulk on a schedule.
 *
 * @WebformHandler(
 *   id = "webform_bulk_email",
 *   label = @Translation("Bulk Email"),
 *   category = @Translation("Notification"),
 *   description = @Translation("Sends webforms submissions in bulk via email."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class BulkEmailWebformHandler extends EmailWebformHandler {

  /**
   * The webform submission queue.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The entity.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The datetime.time service
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The webform submission exporter.
   *
   * @var \Drupal\webform\WebformSubmissionExporterInterface
   */
  protected $submissionExporter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->queueFactory = $container->get('queue');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->time = $container->get('datetime.time');
    $instance->submissionExporter = $container->get('webform_submission.exporter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $theme = parent::getSummary();

    $times = $this->getTimeOptions();
    $frequency = $this->getFrequencyOptions();
    $theme['#settings']['schedule'] = $this->t(':frequency after :time', [
      ':frequency' => $frequency[$this->configuration['scheduled_frequency']],
      ':time' => $times[$this->getScheduledHour()],
    ]);

    $last_sent = $this->webform->getThirdPartySetting('webform_bulk_email', $this->handler_id);
    if (!empty($last_sent)) {
      $theme['#settings']['last_sent'] = \Drupal::service('date.formatter')->format($last_sent, 'short');
    }

    return $theme;
  }

  /**
   * Get configuration default values.
   *
   * @return array
   *   Configuration default values.
   */
  protected function getDefaultConfigurationValues() {
    if (isset($this->defaultValues)) {
      return $this->defaultValues;
    }

    $webform_settings = $this->configFactory->get('webform.settings');
    $site_settings = $this->configFactory->get('system.site');
    $body_format = ($this->configuration['html']) ? 'html' : 'text';
    $default_to_mail = $webform_settings->get('mail.default_to_mail') ?: $site_settings->get('mail') ?: ini_get('sendmail_from');
    $default_from_mail = $webform_settings->get('mail.default_from_mail') ?: $site_settings->get('mail') ?: ini_get('sendmail_from');

    $this->defaultValues = [
      'states' => [WebformSubmissionInterface::STATE_COMPLETED],
      'scheduled_frequency' => '0',
      'scheduled_time' => '0',
      'to_mail' => $default_to_mail,
      'to_options' => [],
      'cc_mail' => $default_to_mail,
      'cc_options' => [],
      'bcc_mail' => $default_to_mail,
      'bcc_options' => [],
      'from_mail' => $default_from_mail,
      'from_options' => [],
      'from_name' => $webform_settings->get('mail.default_from_name') ?: $site_settings->get('name'),
      'subject' => 'Webform submissions for: [webform:id]',
      'header_text' => '',
      'header_html' => '',
      'body' => $this->getBodyDefaultValues($body_format),
      'reply_to' => $webform_settings->get('mail.default_reply_to') ?: '',
      'return_path' => $webform_settings->get('mail.default_return_path') ?: '',
      'sender_mail' => $webform_settings->get('mail.default_sender_mail') ?: '',
      'sender_name' => $webform_settings->get('mail.default_sender_name') ?: '',
      'theme_name' => '',
      'attach_csv' => FALSE,
    ];

    return $this->defaultValues;
  }

  /**
   * Get message body default values, which can be formatted as text or html.
   *
   * @param string $format
   *   If a format (text or html) is provided the default value for the
   *   specified format is return. If no format is specified an associative
   *   array containing the text and html default body values will be returned.
   *
   * @return string|array
   *   A single (text or html) default body value or an associative array
   *   containing both the text and html default body values.
   */
  protected function getBodyDefaultValues($format = NULL) {
    $formats = [
      'text' => "\n[webform_submission:user]: [webform_submission:url] ([webform_submission:created])",
      'html' => '<p>[webform_submission:user]: <a href="[webform_submission:url]">View Submission</a> ([webform_submission:created])</p>',
    ];
    return ($format === NULL) ? $formats : $formats[$format];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $default_values = $this->getDefaultConfigurationValues();

    $form = [
      'schedule' => [
        '#type' => 'details',
        '#title' => $this->t('Send Scheduling'),
        '#open' => TRUE,
        'scheduled_frequency' => [
          '#type' => 'select',
          '#title' => $this->t('Frequency'),
          '#required' => TRUE,
          '#options' => $this->getFrequencyOptions(),
          '#default_value' => $this->configuration['scheduled_frequency'] ?? $default_values['scheduled_frequency'],
        ],
        'scheduled_time' => [
          '#type' => 'select',
          '#title' => $this->t('After this time'),
          '#required' => TRUE,
          '#options' => $this->getTimeOptions(),
          '#default_value' => $this->getScheduledHour() ?? $default_values['scheduled_time'],
        ],
      ],
    ] + $form;

    $form['message'] = [
      'subject' => $form['message']['subject'],
      'header_text' => [
        '#type' => 'webform_codemirror',
        '#format' => '',
        '#title' => $this->t('Header'),
        '#default_value' => $this->configuration['header_text'] ?? $default_values['header_text'],
        '#states' => [
          'visible' => [
            ':input[name="settings[html]"]' => ['checked' => FALSE],
          ],
        ],
      ],
      'header_html' => [
        '#type' => 'webform_html_editor',
        '#format' => '',
        '#title' => $this->t('Header'),
        '#default_value' => $this->configuration['header_html'] ?? $default_values['header_html'],
        '#states' => [
          'visible' => [
            ':input[name="settings[html]"]' => ['checked' => TRUE],
          ],
        ],
      ],
    ] + $form['message'];

    $form['attach_csv'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Attach CSV'),
      '#description' => $this->t('Attach a CSV file with the bulk webform results. The fields selected above will be included.'),
      '#default_value' => $this->configuration['attach_csv'] ?? $default_values['attach_csv'],
    ];
    if (!$this->supportsAttachments()) {
      $form['attach_csv']['#disabled'] = TRUE;
      $form['attach_csv']['#description'] = $this->t('Your mail configuration does not support file attachments');
      $form['attach_csv']['#default_value'] = FALSE;
    }

    // Since this is bulk, we remove the e-mail specific elements.
    unset($form["message"]["subject"]["#options"]["Elements"]);
    unset($form['to']['to_mail']['to_mail']['#options']['Elements']);
    unset($form['to']['cc_mail']['cc_mail']['#options']['Elements']);
    unset($form['to']['bcc_mail']['bcc_mail']['#options']['Elements']);
    unset($form['from']['from_mail']['from_mail']['#options']['Elements']);
    unset($form['from']['from_name']['from_name']['#options']['Elements']);
    unset($form['reply_to']['reply_to']['reply_to']['#options']['Elements']);
    unset($form['additional']['return_path']['return_path']['#options']['Elements']);
    unset($form['additional']['sender_mail']['sender_mail']['#options']['Elements']);
    unset($form['additional']['sender_name']['sender_name']['#options']['Elements']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    if (isset($values['message']['header_text'])) {
      $this->configuration['header_text'] = $values['message']['header_text'];
    }
    if (isset($values['message']['header_html'])) {
      $this->configuration['header_html'] = $values['message']['header_html'];
    }
    if (isset($values['schedule']['scheduled_frequency'])) {
      $this->configuration['scheduled_frequency'] = $values['schedule']['scheduled_frequency'];
    }
    if (isset($values['schedule']['scheduled_time'])) {
      $this->setScheduledHour($values['schedule']['scheduled_time']);
    }
    if (isset($values['attach_csv'])) {
      $this->configuration['attach_csv'] = $values['attach_csv'];
    }
  }

  /**
   * Get the submission queue.
   *
   * @return \Drupal\Core\Queue\QueueInterface
   *   The submission queue.
   */
  public function getQueue() {
    return $this->queueFactory->get('webform_bulk_email_submission_queue.' . $this->webform->id() . '.' . $this->handler_id);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $state = $webform_submission->getWebform()->getSetting('results_disabled') ? WebformSubmissionInterface::STATE_COMPLETED : $webform_submission->getState();
    if ($this->configuration['states'] && in_array($state, $this->configuration['states'])) {
      $this->getQueue()->createItem($webform_submission->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postDelete(WebformSubmissionInterface $webform_submission) {
    if (in_array(WebformSubmissionInterface::STATE_DELETED, $this->configuration['states'])) {
      $this->getQueue()->createItem($webform_submission->id());
    }
  }

  /**
   * Send an aggregated e-mail of all eligible submissions.
   */
  public function sendBulk() {
    $webform = $this->getWebform();
    $queue = $this->getQueue();
    $wss = $this->entityTypeManager->getStorage('webform_submission');
    $message = NULL;
    $ids = [];
    while ($item = $queue->claimItem()) {
      /** @var WebformSubmissionInterface $webform_submission */
      $webform_submission = $wss->load($item->data);
      if (empty($message)) {
        $message = $this->getMessage($webform_submission);
        $message['body'] = [$message['body']];
      }
      else {
        $message['body'][] = $this->getMessage($webform_submission)['body'];
      }
      $ids[] = $webform_submission->id();
      $queue->deleteItem($item);
    }

    if (empty($ids)) {
      // Nothing to send.
      return;
    }

    $attach_csv = $this->configuration['attach_csv'];
    if (!empty($attach_csv) && $this->supportsAttachments()) {
      // Calculate min and max submission ids from the queue.
      $min = min($ids);
      $max = max($ids);

      $excluded_columns = $this->configuration['excluded_elements'] + ['serial', 'uuid', 'token', 'completed', 'changed', 'in_draft', 'current_page', 'remote_addr', 'uid', 'langcode', 'webform_id', 'entity_type', 'entity_id'];
      $default_options = $this->submissionExporter->getDefaultExportOptions();
      // @TODO Allow these options to be configurable.
      $csv_options = [
        'exporter' => 'delimited',
        'delimiter' => ',',
        'excel' => TRUE,
        'multiple_delimiter' => ';',
        'header_format' => 'label',
        'header_prefix' => FALSE,
        'header_prefix_label_delimiter' => ': ',
        'header_prefix_key_delimiter' => '__',
        'options_single_format' => 'compact',
        'options_multiple_format' => 'compact',
        'options_item_format' => 'label',
        'entity_reference_items' => ['id', 'title', 'url'],
        'excluded_columns' => array_combine($excluded_columns, $excluded_columns),
        'files' => FALSE,
        'range_type' => 'sid',
        'range_latest' => '',
        'order' => 'asc',
        'sticky' => FALSE,
        'state' => 'all',
        'range_start' => $min,
        'range_end' => $max,
      ];
      $export_options = $csv_options + $default_options;

      $this->submissionExporter->setWebform($webform);
      $this->submissionExporter->setExporter($export_options);
      $this->submissionExporter->generate();
      $attachment = $this->submissionExporter->getExportFilePath();
      $message['attachments'][] = [
        'filepath' => $attachment,
        'filemime' => 'text/csv',
      ];
    }

    if (!empty($webform_submission) && !empty($message)) {
      $message['body'] = $this->replaceTokens($this->configuration['html'] ? $this->configuration['header_html'] : $this->configuration['header_text'], $webform_submission) . implode('', $message['body']);
      $this->sendMessage($webform_submission, $message);
    }
  }

  /**
   * Set zone adjusted time.
   *
   * @param int $num
   *   The hour to set. This is adjusted using the user's timezone.
   */
  public function setScheduledHour($num) {
    $now = $this->time->getRequestTime();
    $offset = (strtotime('UTC', $now) - $now) / 3600;
    $this->configuration['scheduled_time'] = ($num + $offset + 24) % 24;
  }

  /**
   * Get zone adjusted time.
   *
   * @return int
   *   The hour adjusted by the users's timezone.
   */
  public function getScheduledHour() {
    $now = $this->time->getRequestTime();
    $offset = (strtotime('UTC', $now) - $now) / 3600;
    return (($this->configuration['scheduled_time'] ?? 0) - $offset + 24) % 24;
  }

  /**
   * Check on the cron if it's time to send an e-mail.
   */
  public function cron() {
    $now = $this->time->getRequestTime();
    $old_timestamp = $this->webform->getThirdPartySetting('webform_bulk_email', $this->handler_id) ?? strtotime('-24 hours UTC', $now);
    $next_timestamp = strtotime('tomorrow ' . $this->configuration['scheduled_time'] . ':00 UTC', $old_timestamp);

    if ($next_timestamp <= $now) {
      $this->sendBulk();
      $this->webform->setThirdPartySetting('webform_bulk_email', $this->handler_id, $now);
      $this->webform->save();
    }
  }

  /**
   * Get a list of frequency options.
   */
  private function getFrequencyOptions() {
    return [
      'daily' => $this->t('Daily'),
    ];
  }

  /**
   * Get a list of time options.
   */
  private function getTimeOptions() {
    return [
      '0' => $this->t('12 AM'),
      '1' => $this->t('1 AM'),
      '2' => $this->t('2 AM'),
      '3' => $this->t('3 AM'),
      '4' => $this->t('4 AM'),
      '5' => $this->t('5 AM'),
      '6' => $this->t('6 AM'),
      '7' => $this->t('7 AM'),
      '8' => $this->t('8 AM'),
      '9' => $this->t('9 AM'),
      '10' => $this->t('10 AM'),
      '11' => $this->t('11 AM'),
      '12' => $this->t('12 PM'),
      '13' => $this->t('1 PM'),
      '14' => $this->t('2 PM'),
      '15' => $this->t('3 PM'),
      '16' => $this->t('4 PM'),
      '17' => $this->t('5 PM'),
      '18' => $this->t('6 PM'),
      '19' => $this->t('7 PM'),
      '20' => $this->t('8 PM'),
      '21' => $this->t('9 PM'),
      '22' => $this->t('10 PM'),
      '23' => $this->t('11 PM'),
    ];
  }

}
